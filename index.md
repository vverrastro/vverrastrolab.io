---
title: Vito Verrastro
layout: default
tab_item_1: 
tab_item_2: Lines of code
tab_item_3: Mouse click
tab_item_4: 
value_item_1: 
value_item_2: 999999+
value_item_3: 999999-
value_item_4: 
---
```javascript
function welcome() {
  console.log("Hi, I'm Vito Verrastro");
}
``` 
A creative Software Engineer with extensive experience in software development quickly become a technical point of reference inside the team for different complex technical assignment. Currently DevRel and one of NLU Coordinator in the Bixby Italy team set up to complete the
development of Samsung's voice assistant. 
<br><br>Able to organize and lead a team, manage requests at multiple levels (business, customers, development) and finally focus on the objectives to be achieved, always providing valid and timely
solutions.
<br><br>Passionate about AI and Natural Language Processing.